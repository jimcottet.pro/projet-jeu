package co.simplon.promo18.Characters;

import java.util.Scanner;
import co.simplon.promo18.Tools.Printer;

public class TobBylan {
  Printer printer = new Printer();
  Scanner  scanner = new Scanner(System.in);

  public Hero speaking(Hero hero) {
    System.out.print("[🪕 Tob Bylan] ");
    printer.textHumanizedAuto("Ah un Aventurier !", false);
    System.out.print("[🪕 Tob Bylan] ");
    printer.textHumanizedAuto("Je suis... un... un barde...", false);
    System.out.print("[🪕 Tob Bylan] ");
    printer.textHumanizedAuto("Si tu réussi à terminer ma chanson..."
     + "Je te donne...", true);
    printer.textHumanizedAuto("Tob regarde autours de lui, puis il voit dans sa main " + 
    "une bouteille d'alcool à moitié pleine...", true);
    System.out.print("[🪕 Tob Bylan] ");
    printer.textHumanizedAuto("Je te donne ma bouteille !", true);
    printer.textHumanizedAuto("Acceptez-vous ? [1 = Oui] [2 = Non]", true);
    int answer = scanner.nextInt();
    if (answer == 1) {
      Boolean win = false;
      System.out.print("[🪕 Tob Bylan] ");
      printer.textHumanizedInLine("Com", 60, 0, 750);
      printer.textHumanizedInLine("bien de routes", 80, 50, 750);
      printer.textHumanizedInLine(" doit parcourir un homme,\n", 80, 50, 750);
      printer.textHumanizedInLine("avant", 80, 0, 750);
      printer.textHumanizedInLine(" d'être appelé, ", 80, 50, 750);
      printer.textHumanizedInLine("un homme ?\n", 80, 100, 750);
      printer.textHumanizedInLine("Com", 60, 0, 750);
      printer.textHumanizedInLine("bien de mers", 80, 50, 750);
      printer.textHumanizedInLine(" doit parcourir une colombe,\n", 80, 50, 750);
      printer.textHumanizedInLine("avant", 80, 0, 750);
      printer.textHumanizedInLine(" de s'endormir ", 80, 50, 750);
      printer.textHumanizedInLine("dans le...\n", 80, 100, 750);
      System.out.print("[🪕 Tob Bylan] ");
      printer.textHumanizedAuto("alors ?" , true);
      while (!win) {
        String lyrics = scanner.nextLine();
        if (lyrics.equals("sable")) {
          System.out.print("[🪕 Tob Bylan] ");
          printer.textHumanizedAuto("C'est bien ça !", false);
          System.out.print("[🪕 Tob Bylan] ");
          printer.textHumanizedAuto("Tiens ! Voila ma bouteille 🍾", true);
          win = true;
          hero.bottle = true;
        }
      }
    } else {
      printer.textHumanizedAuto("Vous courrez vers la porte et ignorez Tob Bylan", true);
    }
    return hero;
  }
}
