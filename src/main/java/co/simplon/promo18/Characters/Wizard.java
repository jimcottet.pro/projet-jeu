package co.simplon.promo18.Characters;

import java.util.Random;
import java.util.Scanner;
import co.simplon.promo18.Games.GuessAnagramme;
import co.simplon.promo18.Tools.Printer;

public class Wizard {

  Printer printer =  new Printer();
  GuessAnagramme guessAnagramme = new GuessAnagramme();
  Scanner scanner = new Scanner(System.in);
  Random rand = new Random();

  public Hero speaking(Hero hero) {
    System.out.print("[🧙‍♂️ Magicien] ");
    printer.textHumanizedAuto(" Qui es-tu étranger ?", false);
    System.out.print("[🧙‍♂️ Magicien] ");
    printer.textHumanizedAuto(" Un " + hero.name + " apparemment...", false);
    System.out.print("[🧙‍♂️ Magicien] è");
    printer.textHumanizedAuto(" Veux-tu jouer à un peit jeu ?", true);
    printer.textHumanizedAuto("Tapez 1 pour dire \"Oui\" et 2 pour dire \"Non\"", true);
    int answer = scanner.nextInt();
    while (answer != 1) {
      answer = scanner.nextInt();
      int number = rand.nextInt(4);
      if (answer == 2) {
        switch (number) {
          case 0:
          System.out.print("[🧙‍♂️ Magicien] :");
          printer.textHumanizedAuto("Mais il est vraiment bien...", false);
          break;
          case 1:
          System.out.print("[🧙‍♂️ Magicien] :");
          printer.textHumanizedAuto("Allez...", false);
          break;
          case 2:
          System.out.print("[🧙‍♂️ Magicien] :");
          printer.textHumanizedAuto("Mais allez...", false);
          break;
          case 3:
          System.out.print("[🧙‍♂️ Magicien] :");
          printer.textHumanizedAuto("Essaie au moins...", false);
          break;
          case 4:
          System.out.print("[🧙‍♂️ Magicien] :");
          printer.textHumanizedAuto("S'il te plait...", false);
          break;
        }
      }
    }
    guessAnagramme.guessAnagramme();
    return hero;
  }
}
