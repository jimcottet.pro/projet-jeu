package co.simplon.promo18.Characters;

import java.util.Random;
import co.simplon.promo18.Tools.Printer;

public class Hero {

  Printer printer = new Printer();
  Random rand = new Random();

  public String name;
  public int life;
  public boolean bottle = false;
  private boolean winner = false;

  public Hero(String name, int life) {
    this.name = name;
    this.life = life;
  }
  public void looseLife(int loss) {
    this.life -= loss;
    if (loss == 1) {
      printer.textHumanizedAuto("Vous venez de perdre " + loss + " point de vie", false);
    } else {
      printer.textHumanizedAuto("Vous venez de perdre " + loss + " points de vie", false);
    }
    printer.textHumanizedAuto("Il vous reste " + this.life + " points de vie", true);
  }
  public int hit() {
    int dammages = rand.nextInt(10);
    if (bottle) {
      dammages += 5;
      if (dammages < 10 ) {
      printer.textHumanizedAuto("Votre attaque est peu puissante...", false);
      printer.textHumanizedAuto("Avec votre bouteille vous infligez " + dammages + " points de dégat", true);
      } else if (dammages >= 11) {
      printer.textHumanizedAuto("Votre attaque est puissante...", false);
      printer.textHumanizedAuto("Avec votre bouteille vous infligez " + dammages + " points de dégat" , true);
      }
      return dammages;
    } else {
      if (dammages < 5 ) {
      printer.textHumanizedAuto("Votre attaque est peu puissante...", false);
      printer.textHumanizedAuto("Vous infligez " + dammages + " points de dégat", true);
      } else if (dammages >= 5) {
      printer.textHumanizedAuto("Votre attaque est puissante...", false);
      printer.textHumanizedAuto("Vous infligez " + dammages + " points de dégat", true);
      }
      return dammages;
    }
  }
  public void setWinning(boolean setWin) {
    this.winner = setWin;
  }
  public boolean getWinner() {
    return winner;
  }
}
