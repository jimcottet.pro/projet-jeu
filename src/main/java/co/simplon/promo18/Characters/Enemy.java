package co.simplon.promo18.Characters;

public class Enemy extends Hero {

  public Enemy(String name, int life) {
    super(name, life);
  }
  public void looseLife(int loss) {
    this.life -= loss;
    if (loss == 1) {
      printer.textHumanizedAuto(name + " vient de perdre " + loss + " point de vie", false);
    } else {
      printer.textHumanizedAuto(name + " vient de perdre " + loss + " points de vie", false);    
    }
    printer.textHumanizedAuto("Il reste à " + this.name + " " + this.life + " points de vie", false);
  }
  public int hit() {
    int dammages = rand.nextInt(10);
    if (dammages < 5 ) {
      printer.textHumanizedAuto("L'attaque de " + this.name + " est peu puissante...", false);
      printer.textHumanizedAuto("Il inflige " + dammages + " points de dégat", true);
    } else if (dammages >= 5) {
      printer.textHumanizedAuto("L'attaque de " + this.name + " est puissante...", false);
      printer.textHumanizedAuto("Il inflige " + dammages + " points de dégat", true);
    }
    return dammages;
  }
}

 
