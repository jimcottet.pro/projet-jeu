package co.simplon.promo18.Tools;

import java.util.Random;

public class Printer {

  Random rand = new Random();

  public void textHumanized(String str, int time, int spaceTime, boolean jumpMoreLine, int waiting) {

    char[] charArray = str.toCharArray();

    for (char x : charArray) {

      int randomize = rand.nextInt(20);

      try {
        if (x == ' ') {
          Thread.sleep(spaceTime - randomize);
          System.out.print(x);
        } else {
          Thread.sleep(time - randomize);
          System.out.print(x);
        }
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    try {
      if (jumpMoreLine) {
        Thread.sleep(waiting);
        System.out.println("\n");
      }else {
        Thread.sleep(waiting);
        System.out.println();
      }

    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  // Texte Humanisé sur une seule ligne
  public void textHumanizedInLine(String str, int time, int spaceTime, int waiting) {

    char[] charArray = str.toCharArray();

    for (char x : charArray) {

      int randomize = rand.nextInt(20);

      try {
        if (x == ' ') {
          Thread.sleep(spaceTime - randomize);
          System.out.print(x);
        } else {
          Thread.sleep(time - randomize);
          System.out.print(x);
        }
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    try {
      Thread.sleep(waiting);
      System.out.print("");
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  // Humanisation automatisée
  public void textHumanizedAuto(String str, boolean jumpMoreLine) {

    char[] charArray = str.toCharArray();

    for (char x : charArray) {

      int randomize = rand.nextInt(30);

      try {
        if (x == ' ') {
          Thread.sleep(50 - randomize);
          System.out.print(x);
        } else {
          Thread.sleep(30 - randomize);
          System.out.print(x);
        }
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    try {
      if (jumpMoreLine) {
        Thread.sleep(500);
        System.out.println("\n");
      } else {
        Thread.sleep(700);
        System.out.println();
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

}
