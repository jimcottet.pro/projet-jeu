package co.simplon.promo18.Adventure;

import java.util.Scanner;
import co.simplon.promo18.Characters.Enemy;
import co.simplon.promo18.Characters.Hero;
import co.simplon.promo18.Games.Fight;
import co.simplon.promo18.Tools.Printer;

public class Tavern {
  
  Printer printer = new Printer();
  Scanner scanner =  new Scanner(System.in);
  Enemy robert = new Enemy("Robert", 30);
  Fight fight = new Fight();
  End end = new End();
  
    public Hero enterTavern(Hero hero) {
      printer.textHumanizedAuto("Vous ouvrez la porte et découvrez une Taverne bondée.", false);
      printer.textHumanizedAuto("Des dizaines de clients s'y entassent," 
       + "ivres, certains dansent même sur les tables", false);
      printer.textHumanizedAuto("Vous décidez de vous approcher du comptoire.", true);
      printer.textHumanizedAuto("Le Tavernier vous interpelle", true);
      System.out.print("[🍻 Tavernier] ");
      printer.textHumanizedAuto("Vous Voulez boire quelque-chose ?", true);
      printer.textHumanizedAuto("Vous commandez un bière et commencez à la siroter tranquillement.", false);
      printer.textHumanizedAuto("Après quelques minutes vous voyez un homme se diriger vers vous.", false);
      printer.textHumanizedAuto("Dès qu'il se trouve à portée, il commence à vous pousser violemment", true);
      System.out.print("[🙎‍♂️ Robert] ");
      printer.textHumanizedAuto("Salut ! Moi c'est Robert... Tu veux te battre ?", true);
      printer.textHumanizedAuto("[1 = Oui] [2 = Non]", true);
      int choice = scanner.nextInt();
      if (choice == 1) {
        System.out.print("[🙎‍♂️ Robert] ");
        printer.textHumanizedAuto("Super !", true);
      } else if (choice == 2) {
        System.out.print("[🙎‍♂️ Robert] ");
        printer.textHumanizedAuto("Tant pis... On va quand même se battre !", true);
      } else {
        System.out.print("[🙎‍♂️ Robert] ");
        printer.textHumanizedAuto("Hein... ?", true);
      }
      System.out.println(hero.bottle);
      hero = fight.goFight(hero, robert, choice);

      System.out.println("\033[H\033[2J");
      return hero; 
    }

}
