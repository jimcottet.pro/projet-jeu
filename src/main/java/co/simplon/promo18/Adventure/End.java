package co.simplon.promo18.Adventure;

import co.simplon.promo18.Characters.Hero;
import co.simplon.promo18.Tools.Printer;

public class End {
  Printer printer = new Printer();

  public void ending(Hero hero) {
    printer.textHumanizedAuto("Vous venez de terminer l'aventure !", true);
    System.out.println("\033[H\033[2J");
  }
}
