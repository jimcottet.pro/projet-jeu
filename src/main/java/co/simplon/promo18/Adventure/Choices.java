package co.simplon.promo18.Adventure;

import java.util.Scanner;

import co.simplon.promo18.Characters.Hero;
import co.simplon.promo18.Tools.Printer;

public class Choices {

  Scanner scanner = new Scanner(System.in);

  public Hero choiceCharac() {

    Hero hero = new Hero("", 0);
    Printer printer = new Printer();

    printer.textHumanizedAuto("\nBienvenue dans cette petite aventure textuelle !", true);
    printer.textHumanizedAuto("Choisissez votre personnage :", true);
    printer.textHumanizedAuto("Tapez: 1 pour le Nain, 2 pour l'Elfe et 3 pour l'homme", true);
    int choice = scanner.nextInt();
    switch (choice) {
      case 1:
        hero = new Hero("Nain", 30);
        printer.textHumanizedAuto("\nPour cette aventure, vous serez donc un Nain !",
            true);
        break;
      case 2:
        hero = new Hero("Elfe", 20);
        printer.textHumanizedAuto(
            "Vous avez choisi l'Elfe !", true);
        break;
      case 3:
        hero = new Hero("Homme", 30);
        printer.textHumanizedAuto("Vous avez choisi l'Homme !", true);
        break;
      default:
        System.out.println("Cette classe n'existe pas... veuillez recommencer...");
    }
    printer.textHumanizedAuto("Vous possèdez : " + hero.life + " points de vie", false);
    printer.textHumanizedAuto(
        "Maintenant que votre personnage a été créé, l'aventure peut commencer !", true);
    // Effacer la console...
    System.out.println("\033[H\033[2J");

    return hero;
  }

}
