package co.simplon.promo18.Adventure;

import java.util.Scanner;
import co.simplon.promo18.Adventure.Alleys.AlleyOne;
import co.simplon.promo18.Adventure.Alleys.AlleyTwo;
import co.simplon.promo18.Characters.*;
import co.simplon.promo18.Tools.*;

public class Awakening {

  Printer printer = new Printer();
  Scanner scanner = new Scanner(System.in);
  AlleyOne alleyOne = new AlleyOne();
  AlleyTwo alleyTwo = new AlleyTwo();

  public Hero awake(Hero hero) {

    printer.textHumanizedAuto(
        "Vous vous réveillez dans une grande salle sombre, haute de plafond, et remplie de colonnades",
        true);
    if (hero.name.equals("Nain")) {
      printer.textHumanizedAuto(
          "Depuis votre petite taille de Nain, vous ne discernez clairement que les ombres"
              + " mouvantes refletées par les flammes des torches",
          true);
    } else if (hero.name.equals("Elfe")) {
      printer.textHumanizedAuto("Votre vue aiguisée d'Elfe vous permet de discerner clairment"
          + " les somptueux motifs gravés sur les pierres", true);
    } else {
      printer.textHumanizedAuto("Debout sur vos deux solides jambes de guerrier vous regardez "
          + "les grandes voutes qui parcourent les hauteurs de la pièce", true);
    }

    printer.textHumanizedAuto("\nAprès un instant...", true);
    printer.textHumanizedAuto(
        "Vous vous dirigez vers le centre de la pièce... dans la pénombre vous distinguez trois portes :",
        true);
    printer.textHumanizedAuto("L'une à droite, couverte de poussière(porte1)", false);
    printer.textHumanizedAuto("L'autre, à gauche, comme neuve et bien visée sur ses gonds(porte2) ",
        false);
    printer.textHumanizedAuto("Et enfin, au centre, une grande porte entre-ouverte..."
        + " d'ailleurs vous croyez voir passer un peu de lumière...(porte3)", true);


    boolean open = false;
    while (open != true) {
      String choice = scanner.nextLine();
      if (choice.equals("porte1")) {
        System.out.println("\033[H\033[2J");
        hero = alleyOne.openDoor1(hero);
        open = true;
      } else if (choice.equals("porte2")) {
        printer.textHumanizedAuto("Cette porte est fermée et trop solide pour être enfoncée..."
            + " choisissez-en une autre...", true);
      } else if (choice.equals("porte3")) {
        System.out.println("\033[H\033[2J");
        hero = alleyTwo.openDoor2(hero);
        open = true;
      } else {
        printer.textHumanizedAuto("êtes vous un idiot ?... Ceci n'est pas une porte !?", true);
      }
    }
    // Effacer la console...
    return hero;
  }
}

