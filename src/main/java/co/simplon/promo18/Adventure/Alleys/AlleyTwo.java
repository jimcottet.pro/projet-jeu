package co.simplon.promo18.Adventure.Alleys;

import java.util.Scanner;
import co.simplon.promo18.Characters.Hero;
import co.simplon.promo18.Characters.TobBylan;
import co.simplon.promo18.Tools.Printer;

public class AlleyTwo {

  Printer printer = new Printer();
  Scanner scanner = new Scanner(System.in);
  TobBylan tobBylan = new TobBylan();

  public Hero openDoor2(Hero hero) {
    printer.textHumanizedAuto("Vous poussez la porte entr'ouverte avec une grande" +
     " facilité, malgré son poids.", false);
    printer.textHumanizedAuto("Le couloirs est très éclairé et ressemble à un labyrithe" + 
     " tant il semble y avoir de changements de direction", true);
    printer.textHumanizedAuto("Après un certain temps à marcher, " +
    "vous percevez un son de guitar qui se fait de plus en plus proche", false);

    hero =  tobBylan.speaking(hero);

    System.out.println("\033[H\033[2J");
    
    return hero;
  }
}
