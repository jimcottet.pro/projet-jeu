package co.simplon.promo18.Adventure.Alleys;

import co.simplon.promo18.Adventure.Tavern;
import co.simplon.promo18.Characters.Hero;
import co.simplon.promo18.Characters.Wizard;
import co.simplon.promo18.Tools.Printer;

public class AlleyOne {

  Printer printer = new Printer();
  Wizard wizard = new Wizard();
  Tavern tavern = new Tavern();

  public Hero openDoor1(Hero hero) {
    printer.textHumanizedAuto("Vous ouvrez cette vieille porte..."
        + "Elle est si abimée que son bois manque de se rompre dans vos mains", false);
    printer.textHumanizedAuto("Le couloir semble long... vous entendez au loin l'écho de vos pas...", true);
    printer.textHumanized("clap... clap... clap...", 100, 1000, true, 2000);
    printer.textHumanizedAuto("Après quelques minutes de marche, vous entendez comme un murmure au loin...", true);
    printer.textHumanizedAuto("Le murmure se raproche...", true);
    printer.textHumanizedAuto("...", true);
    printer.textHumanizedAuto("C'est un Magicien !", true);

    hero = wizard.speaking(hero);

    printer.textHumanizedAuto("Vous avez répondu à l'énigme du magicien !", false);
    printer.textHumanizedAuto("Courrez vite avant qu'il ne vous en pose une autre", true);

    System.out.println("\033[H\033[2J");

    return hero;
  }

}
