package co.simplon.promo18.Games;


import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import co.simplon.promo18.Tools.Printer;

public class GuessAnagramme {

  Random rand = new Random();
  Scanner scanner = new Scanner(System.in);
  Printer printer = new Printer();

  public void guessAnagramme() {

    boolean win = false;
    String text = randWords();
    String textLowCase = text.toLowerCase();
    System.out.print("[🧙‍♂️ Magicien] :");
    printer.textHumanizedAuto("Tu dois trouver un Anagramme du mot : " + text, true);
    System.out.print("[🧙‍♂️ Magicien] :");
    printer.textHumanizedAuto("Vas-y !", true);

    while (win != true) {
      String textToCompare = scanner.nextLine();
      System.out.println();
      String textToCompareLowCase = textToCompare.toLowerCase();
      char[] liste = textToCompareLowCase.toCharArray();
      int x = 0;
      int j = 0;
      ArrayList<Character> letters = new ArrayList<Character>();
      for (char i : liste) {
        j++;
        if (textLowCase.contains(Character.toString(i))) {
          if (!letters.contains(Character.toString(i))) {
            letters.add(i);
            x++;
          }
        }
      }
      if (x == textLowCase.length() && !textLowCase.equals(textToCompareLowCase) && x == j) {
        System.out.print("[🧙‍♂️ Magicien] :");
        printer.textHumanizedAuto("Tu as trouvé ! " + textToCompare + " est bien un Anagramme de " + text, true);
        System.out.print("[🧙‍♂️ Magicien] :");
        printer.textHumanizedAuto("tu peux passer !", true);
        win = true;
      } else if (textLowCase.equals(textToCompareLowCase)) {
        System.out.print("[🧙‍♂️ Magicien] :");
        printer.textHumanizedAuto("Ne me prends pas pour un idiot...", false);
        System.out.print("[🧙‍♂️ Magicien] :");
        printer.textHumanizedAuto("Recommence...", true);
      } else {
        System.out.print("[🧙‍♂️ Magicien] :");
        printer.textHumanizedAuto("Faux ! " + textToCompare + 
        " n'est pas un Anagramme de " + text,
            true);
        System.out.print("[🧙‍♂️ Magicien] :");
        printer.textHumanizedAuto("Essaie encore !", true);
      }
    }
  }

  public String randWords() {
    ArrayList<String> wordList = new ArrayList<String>();
    int randNumber = rand.nextInt(3);
    wordList.add("Marion");
    wordList.add("Aimer");
    wordList.add("Onirique");
    String guessWord = wordList.get(randNumber);
    return guessWord;
  }

}
