package co.simplon.promo18.Games;

import co.simplon.promo18.Characters.Enemy;
import co.simplon.promo18.Characters.Hero;
import co.simplon.promo18.Tools.Printer;

public class Fight {

  Printer printer = new Printer();

  public Hero goFight(Hero hero, Enemy enemy, int choice) {
    int hit;
    // Premier coup...
    if (choice == 1) {
      hit = hero.hit();
      enemy.looseLife(hit);
    } else {
      hit = enemy.hit();
      hero.looseLife(hit);
    }
    int round = 0;
    while (true) {
      round++;
      printer.textHumanizedAuto("\nTour " + round + "\n", false);
      hit = enemy.hit();
      hero.looseLife(hit);
      if (hero.life <= 0){
        printer.textHumanizedAuto("Vous avez perdu", true);
        printer.textHumanizedAuto("Après avois pris autant de coups, vous tombez dans le pommes...", true);
        break;
      }
      hit = hero.hit();
      enemy.looseLife(hit);
      if (enemy.life <= 0){
        printer.textHumanizedAuto("Vous avez gagné !", true);
        hero.setWinning(true);
        break;
      }
    }
    return hero;
  }
}

