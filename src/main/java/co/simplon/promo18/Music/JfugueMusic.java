package co.simplon.promo18.Music;

import org.jfugue.player.Player;

public class JfugueMusic {
  Player player = new Player();
  public void playMusic() {

      player.play(
          "T140 "
              // Celesta
              + "V1 I[Celesta] F3w G3w A3w F3w | G3w A3w F3w G3w | F3w G3w A3w F3w | G3w A3w F3w G3w |  "
              // Guitar
              + "V2 I[Guitar] F5h G5h A5h F5h G5h A5h A5h G5h | F5h G5h A5h F5h G5h A5h G5h F5h | "
              + "F5h G5h A5h F5h G5h A5h A5h G5h | F5h G5h A5h F5h G5h A5h A5h F5h "
              // Clap
              + "V9 [Timpani]w [Timpani]w [Timpani]w [Timpani]h [Timpani]h | "
              + "[Timpani]w [Timpani]w [Timpani]w [Timpani]h [Timpani]h | "
              + "[Timpani]w [Timpani]w [Timpani]w [Timpani]h [Timpani]h | "
              + "[Timpani]w [Timpani]w [Timpani]w [Timpani]h [Timpani]h "
              );

  }

  public void playWake() {
    player.play("T180 "
        + "V1 I[french_horn] C5w E5h D5h C5w A4w | A4w E5h C5h E5w E5w "
        + "V2 I[String_Ensemble_1] C5w E5h D5h C5w A4w | A4w E5h C5h E5w E5w"
    );
  }
}
